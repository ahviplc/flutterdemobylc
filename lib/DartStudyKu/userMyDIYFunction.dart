import 'package:flutter_demo/DartStudyKu/lcDemo.dart';
import 'package:flutter_demo/utils/md5Utils.dart';

/**
 * 使用自定义的函数
 * userMyDIYFunction.dart
 * [LC博客-一加壹博客最Top](http://www.oneplusone.vip)
 * LC 2018-12-10 14:22:44
 */

main(){
  sayHello(name: 'LC');  // 打印 hello, my name is wangwu
  sayHello2(name: 'Ahviplc');
  print(sum(1, 1));
  print(generateMd5("123456"));
}