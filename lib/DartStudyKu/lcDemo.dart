import 'dart:core';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

/**
 * dart例子大全
 * 链接：https://www.imooc.com/article/67042
 * [LC博客-一加壹博客最Top](http://www.oneplusone.vip)
 * LC 2018-12-10 13:07:19
 */

//命名参数
sayHello({String name}) {
  print("hello, my name is $name");
}

sayHello2({name: String}) {
  print("hello, my name is $name");
}

// 位置参数的默认值-参数默认值
int sum(int a, int b, [int c = 3]) {
  return a + b + c;
}

//匿名函数
test(Function callback) {
  callback("hello");
}

//is
//和c#一样， is运算符用于判断一个变量是不是某个类型的数据

// is!则是判断变量不是某个类型的数据
var s = "hello";
var numByLC = 6;

//??= 和 ?.运算符
//??=运算符 如果 ??= 运算符前面的变量为null，则赋值，否则不赋值
var param1 = "hello", param2 = null;

// ?.运算符
var str1 = "hello world";
var str2 = null;

//..运算符（级联操作）
//使用..调用某个对象的方法（或者成员变量）时，返回值是这个对象本身，这样就能方面实现链式调用：
class Person {
  eat() {
    print("I am eating...");
  }

  sleep() {
    print("I am sleeping...");
  }

  study() {
    print("I am studying...");
  }
}

//不一样的try/catch
// try catch语句 demo

//类（Class）
//不需要用private, protected, public等修饰成员变量或成员函数
//
//构造函数可以不用方法体，自动对应

class Person2 {
  String name;
  int age;
  String gender;

  Person2(this.name, this.age, this.gender);

  sayHello() {
    print("hello, this is $name, I am $age years old, I am a $gender");
  }
}

//getter/setter方法
class Rectangle {
  num left, top, width, height; // 构造方法传入left, top, width, height几个参数
  Rectangle(this.left, this.top, this.width,
      this.height); // right, bottom两个成员变量提供getter/setter方法
  num get right => left + width;

  set right(num value) => left = value - width;

  num get bottom => top + height;

  set bottom(num value) => top = value - height;
}

//mixins
//mixins是一个前端很火的概念，是一个重复使用类中代码的方式。

class A {
  a() {
    print("A's a()");
  }
}

class B {
  b() {
    print("B's b()");
  }
} // 使用with关键字，表示类C是由类A和类B混合而构成

class C = A with B;

//异步
//Dart提供了类似ES7中的async await等异步操作，这种异步操作在Flutter开发中会经常遇到，比如网络或其他IO操作，文件选择等都需要用到异步的知识。 async和await往往是成对出现的，如果一个方法中有耗时的操作，你需要将这个方法设置成async，并给其中的耗时操作加上await关键字，如果这个方法有返回值，你需要将返回值塞到Future中并返回，如下代码所示：
//Future checkVersion() async {  var version = await lookUpVersion();  // Do something with version}
// 下面的代码使用Dart从网络获取数据并打印出来：

Future<String> getNetData() async {
  http.Response res = await http.get(
      "http://api.douban.com/v2/movie/top250?start=25&count=2",
      headers: {"Accept": "application/json"});
  return res.body;
}

//main方法
main() {
  // 打印 hello, my name is zhangsan
  sayHello(name: 'zhangsan'); // 打印 hello, my name is wangwu
  sayHello2(name: 'wangwu');
  print(sum(1, 1));

  test((param) {
    // 打印hello
    print(param);
  });

  print(s is String); // true
  print(numByLC is! String); // true

  param1 ??= "world";
  param2 ??= "world";
  print("param1 = $param1"); // param1 = hello
  print("param2 = $param2"); // param2 = world

  print(str1?.length); // 11
  print(str2?.length); // null

  //print(str2.length); // 报错-具体信息如下

  /*Unhandled exception:
  NoSuchMethodError: The getter 'length' was called on null.
  Receiver: null
  Tried calling: length
  #0      Object.noSuchMethod (dart:core/runtime/libobject_patch.dart:50:5)*/

  // 依次打印
  //  I am eating...
  //  I am sleeping...
  //  I am studying...
  new Person()
    ..eat()
    ..sleep()
    ..study();

  // try catch语句
  try {
    print(1 ~/ 0);
  } catch (e) {
    // IntegerDivisionByZeroException
    print(e);
  }

  // try catch语句 捕获指定类型的异常
  try {
    1 ~/ 0;
  } on IntegerDivisionByZeroException {
    // 捕获指定类型的异常
    print("error"); // 打印出error
  } finally {
    print("over"); // 打印出over
  }

  //mixins Test
  C c = new C();
  c.a(); // A's a()
  c.b(); // B's b()

  getNetData().then((str) {
    print(
        '-----------------------------------------------------------------------------');
    print(str);
    print(
        '-----------------------------------------------------------------------------');
    print(json.decode(str));
    print(
        '-----------------------------------------------------------------------------');
    print(json.encode(json.decode(str)));
  });

  print("list-------------------------------------------------------------------------");
  //list List的声明，可以用var也可用List。
// 非固定长度
  var testList = List();
// 也可以 List testList = List();
  print(testList.length); // 0
// 固定长度
  var fixedList = List(2);
  print(fixedList.length); // 2

  testList.add("hello");
  testList.add(123);

  //fixedList.add(1); // 报错。固定长度不能添加元素

  fixedList[0] = 123;
  print(testList[0]); // hello
  print(testList[1]); // 123
// 元素类型固定
  var typeList = List<String>(); // 只能添加字符串类型的元素

  typeList.add("hello"); // 正确
  //typeList.add(1); // 错误。类型不正确

// 直接赋值
  var numList = [1, 2, 3];
  //numList.add("hello"); // 错误，类型不正确
  var dyList = [true, 'hello', 1];

//常用的方法，属性,更多查看：https://api.dartlang.org/stable/2.0.0/dart-core/List-class.html
  var testList2 = [1, '2', 3.1];

// length 属性，返回队列长度
  print(testList2.length); // 3
// isEmpty 属性，返回队列是否为空
  print(testList2.isEmpty); // false
// 添加元素
  testList2.add(4);
  print(testList2); // [1, '2', 3.1, 4]

// 删除元素
  testList2.remove(3.1);
  print(testList2); // [1, '2', 4]

// 元素索引. 列表的索引从0开始
  print(testList2.indexOf(4)); // 2

  print("Set-------------------------------------------------------------------------");
//  Set
//  Set是没有顺序且不能重复的集合，所以不能通过索引去获取值。下面是Set的定义和常用方法

  var testSet = new Set();

  //var testSet2 = new Set(2); // 错误，Set没有固定元素的定义

  print(testSet.length); // 0

  testSet.add(1);
  testSet.add(1); // 重复元素无效
  testSet.add("a");
  print(testSet); // {1, "a"}

  print(testSet.contains(1)); // true

  testSet.addAll(['b', 'c']);

  print(testSet); // (1, 'a', 'b', 'c')

  testSet.remove('b');
  print(testSet); // (1, 'a', 'c')

  print("Map-------------------------------------------------------------------------");
//  Map
//  映射是无序的键值对。下面是常用的属性和方法

// 常用的两种定义方式
  var testMap = Map();

  var testMap2 = {
    "a": "this is a",
    "b": "this is b",
    "c": "this is c"
  };
// 长度属性
  print(testMap.length); // 0
// 获取值
  print(testMap2["a"]); // this is a
// 如果没有key,返回null
  print(testMap["a"]); // null

// 需要注意的是keys 和 values 是属性不是方法
  print(testMap2.keys); // 返回所有key (a, b, c)

  print(testMap2.values); // 返回左右value (this is a, this is b, this is c)

// key:value 的类型可以指定
  var intMap = Map<int, String>();
// map新增元素
  intMap[1] = "Num 1"; // true
  //intMap['a'] = "Char a:; // 错误，类型不正确。
  intMap[2] = "Num 2";
// 删除元素
  intMap.remove(2);
// 是否存在key
  print(intMap.containsKey(1)); // true

  print("list set map-通用方法-------------------------------------------------------------------------");
//  通用方法
//  List、Set和Map有一些通用的方法。其中的一些通用方法都是来自于类Iterable。List和Set是iterable类的实现。
//  虽然Map没有实现Iterable, 但是Map的属性keys和values都是Iterable对象。

// 通用属性 isEmpty和 isNotEmpty
  var testSet3 = Set.from(["a", "b", "c"]);
  var testList3 = [1, 2, 3, 4];
  var testMap3 = Map();

  print(testSet3.isNotEmpty); // true
  print(testList3.isEmpty); // false
  print(testMap3.isEmpty); // true

  testMap3.addAll({
    "zh": "china",
    "us": "usa"
  });

// forEach方法
  testList3.forEach((num) => print("I am num ${num}")); // I am num 1 等等 I am num 2 I am num 3 I am num 4
  testMap3.forEach((k, v) => print("${k} is ${v}")); // zh is china 等等 us is usa

// iterable提供了 map 方法，来处理每一个集合中的对象，并返回一个结果
  var setIter = testSet3.map((v) => v.toUpperCase());
  print(setIter); // (A, B, C)

// 可以用toList和toSet将结果转换成列表或者集合
  var listIter = testSet3.map((v) => v.toUpperCase()).toList();
  print(listIter); // [A, B, C]

// iterable提供了where方法，来过滤集合中的值，并返回一个集合
  var whereList = testList3.where((num) => num > 2).toList();
  print(whereList); // [3, 4]。如果不用toList()则返回(3, 4)

// iterable提供了any方法和every方法，来判断集合中的值是否符合条件，并返回bool
  print(testList3.any((num) => num > 2)); // true
  print(testList3.every((num) => num > 2)); // false



}
